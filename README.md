# Advance Self Driving Car

Advance Self Driving Car is based on combination of multiple prototypes to support self driving in Indian roads. This consist:
  - Lane detection
  - Steering angle prediction using CNN
  - Object detection YOLOv3

# Testing Environment
The project is tested in an environment with Ubuntu 18.04, NVIDIA GTX 1050, CUDA 10.1 and CUDnn 7.5. Anaconda is used for creating verrtual environment with python3. Tensorflow-GPU version is used for running the models over GPU. The video used for testing is taken in Delhi, India.

# Dependencies
  - Python 3+
  - tensorflow 2.0 (Used GPU version for testing)
  - OpenCV 4.2.0 GPU version
  - Numpy
  - Anaconda
  - NVIDIA CUDA 10.1

### Repository tree

```
advance-self-driving-car
	|__ yolov3.py
	|__ runVideo.py
	|__ README.md
	|__ Resources
	|      |__ register.json
	|      |__ steering_wheel_image.jpg
	|      |__ video.mp4
	|__ Output
	|__ Steering
	|      |__ train.py
	|      |__ model.py
	|      |__ driving_data.py
	|	   |__ driving_dataset
	|      |      |__ README.md
	|      |__ save
	|             |__ checkpoint
	|             |__ model.ckpt
	|__ yolo-coco
	       |__ coco.names
	       |__ yolov3.cfg
	       |__ yolov3.weights
```

### Clone repository

- If repo not present
```sh
$ git clone https://gitlab.com/pranjalchanda08/advance-self-driving-car.git
```
 - If repo already present. Pull and checkout latest master branch
```sh
$ cd advance-self-driving-car
$ git pull
$ git checkout master
```
## Pre-requisite
### Install NVIDIA graphics drivers. 

```sh
$ sudo add-apt-repository ppa:graphics-drivers/ppa
$ sudo apt-get install dkms build-essential
$ sudo apt-get update
$ sudo apt-get install nvidia-driver-418
$ reboot
```
 - Install depencent libraries for CUDA installation.
```sh
$ sudo apt-get install freeglut3 freeglut3-dev libxi-dev libxmu-dev
```
```sh
$ cd
$ mkdir cuda; cd cuda
$ mkdir tmp
$ wget http://developer.download.nvidia.com/compute/cuda/10.1/Prod/local_installers/cuda_10.1.243_418.87.00_linux.run
$ sudo sh cuda_10.1.243_418.87.00_linux.run --tmpdir=/home/{user}/cuda/tmp/. --toolkit --samples
```
- This is new installer and is much slower to start-up than the older scripts (in case you have done this before).
- You will be asked to accept the EULA, type "accept" and press Enter, after which you will be presented with a "selector". 

> **Un-check** the "Driver" block and then select "Install" and hit "Enter".
```sh
┌──────────────────────────────────────────────────────────────────────────────┐
│ CUDA Installer                                                               │
│ - [ ] Driver                                                                 │
│      [ ] 418.39                                                              │
│ + [X] CUDA Toolkit 10.1                                                      │
│   [X] CUDA Samples 10.1                                                      │
│   [X] CUDA Demo Suite 10.1                                                   │
│   [X] CUDA Documentation 10.1                                                │
│   Install                                                                    │
│   Options                                                                    │
│                                                                              │
│                                                                              │
│ Up/Down: Move | Left/Right: Expand | 'Enter': Select | 'A': Advanced options │
└──────────────────────────────────────────────────────────────────────────────┘
```
 - Need to add the following set of commands in ~/.bashrc file
 
```sh
export PATH=/usr/local/cuda-10.1/bin:$PATH
export CUDADIR=/usr/local/cuda-10.1
export LD_LIBRARY_PATH=/usr/local/cuda-10.1/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/local/cuda/extras/CUPTI/lib64:$LD_LIBRARY_PATH
```

### Install Tensorflow
 - Make sure all the above steps are covered. 
 - Now install tensorflow-gpu

```sh
$ conda activate <venv>
(venv)$ pip install tensorflow
```
 -  Test your tensorflow
 -  Open python in your venv
```py
import tensorflow as tf
tf.config.list_physical_devices('GPU')
```
 - The above shall list available GPUs.

### Install OpenCV GPU build
 - **NOTE:** Avoid copying directly from below. Some commands have hyper-parameters that needs to be set accordingly.
 - Detailed reference: [OpenCV’s “dnn” module with NVIDIA GPUs](https://www.pyimagesearch.com/2020/02/03/how-to-use-opencvs-dnn-module-with-nvidia-gpus-cuda-and-cudnn/)
```sh
$ sudo apt-get update
$ sudo apt-get upgrade
$ sudo apt-get install build-essential cmake unzip pkg-config
$ sudo apt-get install libjpeg-dev libpng-dev libtiff-dev
$ sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev
$ sudo apt-get install libv4l-dev libxvidcore-dev libx264-dev
$ sudo apt-get install libgtk-3-dev
$ sudo apt-get install libatlas-base-dev gfortran
$ sudo apt-get install python3-dev
```
 - Download openCV src files. Then build it.
```sh
$ cd
$ mkdir opencv
$ cd opencv
$ wget -O opencv.zip https://github.com/opencv/opencv/archive/4.2.0.zip
$ wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/4.2.0.zip
$ unzip opencv.zip
$ unzip opencv_contrib.zip
$ mv opencv-4.2.0 opencv
$ mv opencv_contrib-4.2.0 opencv_contrib
$ rm *.zip
```
 - Now configure OpenCV src files w.r.t specific CUDA GPU Architecture. 
 - To Know about the {GPU_ARCH}, Please visit: [CUDA-GPUs](https://developer.nvidia.com/cuda-gpus)
 - Scroll down and search for your GPU.

<p align="center"><img src="Output/GPU_ARCH.png" width=676 height=450></p>

```sh
$ cd ~/opencv/opencv
$ mkdir build
$ cd build
$ cmake -D CMAKE_BUILD_TYPE=RELEASE \
	-D CMAKE_INSTALL_PREFIX=/usr/local \
	-D INSTALL_PYTHON_EXAMPLES=ON \
	-D INSTALL_C_EXAMPLES=OFF \
	-D OPENCV_ENABLE_NONFREE=ON \
	-D WITH_CUDA=ON \
	-D WITH_CUDNN=ON \
	-D OPENCV_DNN_CUDA=ON \
	-D ENABLE_FAST_MATH=1 \
	-D CUDA_FAST_MATH=1 \
	-D CUDA_ARCH_BIN={GPU_ARCH} \
	-D WITH_CUBLAS=1 \
	-D OPENCV_EXTRA_MODULES_PATH=~/opencv/opencv_contrib/modules \
	-D HAVE_opencv_python3=ON \
	-D PYTHON_EXECUTABLE=~/.conda/envs/{venv}/lib/python3.{version}/site-packages/cv2 \
	-D BUILD_EXAMPLES=ON ..
```
- **make -j{n}** is very specific to the CPU you have. 
- Please check the number of cores present and then provide the value of {n}. 
- For Eg: Core i7 n = 8. Hence the command becomes make -j8
```sh
$ make -j{n}
$ sudo make install
$ sudo ldconfig
```
 - /build/lib/python3/cv2.cpython-36m-x86_64-linux-gnu.so shall be generated post build. 
 - Copy the generated file to {venv_PATH} and re-configure conda environment.
 - Please double check for the name of the cv2.cpython-36m-x86_64-linux-gnu.so before executing the following commands.
```sh
$ cp ~/opencv/opencv/build/lib/python3/cv2.cpython-36m-x86_64-linux-gnu.so ~/.conda/envs/{venv}/lib/python3.{version}/site-packages/cv2/.
$ conda uninstall fontconfig
```
 - Test your installation. Open python in your {venv}:
```py
import cv2
print(cv2.__version__) #Expected output "4.2.0"
```
## Running the code
 - The code supports both video as well as webcam mode. 
 - Download the yolo-coco models before execution. This is a one time step to be done.
 ```sh
 $ cd advance-self-driving-car/yolo-coco
 $ wget https://pjreddie.com/media/files/yolov3.weights
 ```
### To run using video
 - Register the video you want to run in Resources/register.json file. 
 - This file contains detains of the file with the ROI vertices details as well.
```sh
$ conda activate <venv>
(venv)$ cd advance-self-driving-car
(venv)$ python3 runVideo.py --vSrc video
```
### To run using webcam
```sh
$ conda activate <venv>
(venv)$ cd advance-self-driving-car
(venv)$ python3 runVideo.py 
```
### OUTPUT

<p align="center"><img src="Output/output.png" width=676 height=450><img src="Output/lane detection.png" width=676 height=450></p>
<p align="center"><img src="Output/object detection.png" width=676 height=450><img src="Output/Relative distance.png" width=676 height=450></p>

### To Train for steering angle
 - Run the following for downloading and extracting the dataset **[5.2GB, > 1L frames]**:

```sh
$ cd advance-self-driving-car/Steering
$ wget http://ec2-34-192-87-162.compute-1.amazonaws.com/driving_dataset.zip
$ unzip driving_dataset.zip
```
 - Now to start training follow the following commands
```sh
$ conda activate <venv>
(venv)$ cd advance-self-driving-car/Steering
(venv)$ python3 train.py
```
 - Visualisation of training using tensorboard
```sh
$ conda activate <venv>
(venv)$ cd advance-self-driving-car/Steering
(venv)$ tensorboard --logdir=./logs
```
# Known limitation
  - The project is delt with static Region-of-Interest according to the test video. To run any other video it is required to find the RoI cordinates and replace it with current vertices.

# Refernces/Credits
  - Object Detection: [TensorFlow Object detection](https://github.com/tensorflow/models/tree/master/research/object_detection)
  - Lane detection: [Udacity CarND Lane Lines](https://github.com/udacity/CarND-LaneLines-P1)
  - Steering Wheel Prediction: [Autopilot-TensorFlow by Sully Chen](https://github.com/SullyChen/Autopilot-TensorFlow)
  - Distance Estimation: [Planar Distance Estimation](https://github.com/ndrplz/planar-distance-estimation)
  - End-to-End Deep Learning for Self-Driving Cars: [End-to-End Deep Learning for Self-Driving Cars](https://devblogs.nvidia.com/deep-learning-self-driving-cars/)
