import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import scipy

def weight_variable(shape):
	initial = tf.truncated_normal(shape, stddev=0.1)
	return tf.Variable(initial, name = 'W')

def bias_variable(shape):
	initial = tf.constant(0.1, shape=shape)
	return tf.Variable(initial, name = 'b')

def conv2d(x, W, stride, bias, name = 'conv'):
	with tf.name_scope(name):
		act = tf.nn.relu(tf.nn.conv2d(x, W, strides=[1, stride, stride, 1], padding='VALID') + bias)
		tf.summary.histogram("weights", W)
		tf.summary.histogram("biases", bias)
		tf.summary.histogram("activations", act)
		return act  

def fc_layer(input, W, bias, prob, name = 'FCL'):
	with tf.name_scope(name):			
		fc = tf.nn.relu(tf.matmul(input, W) + bias)
		act = tf.nn.dropout(fc, prob)
		tf.summary.histogram("weights", W)
		tf.summary.histogram("biases", bias)
		tf.summary.histogram("activations", act)
		return act

def output(input, W, bias, name = 'Output'):
	with tf.name_scope(name):
		y = tf.multiply(tf.atan(tf.matmul(h_fc4_drop, W_fc5) + b_fc5), 2) #scale the atan output
		tf.summary.histogram("weights", W)
		tf.summary.histogram("biases", bias)
		tf.summary.histogram("output", y)
		return y

x = tf.placeholder(tf.float32, shape=[None, 66, 200, 3])
y_ = tf.placeholder(tf.float32, shape=[None, 1])
keep_prob = tf.placeholder(tf.float32)

x_image = x

#first convolutional layer
W_conv1 = weight_variable([5, 5, 3, 24])
b_conv1 = bias_variable([24])

h_conv1 = conv2d(x_image, W_conv1, 2, b_conv1, name = 'conv1')

#second convolutional layer
W_conv2 = weight_variable([5, 5, 24, 36])
b_conv2 = bias_variable([36])

h_conv2 = conv2d(h_conv1, W_conv2, 2, b_conv2, name = 'conv2')

#third convolutional layer
W_conv3 = weight_variable([5, 5, 36, 48])
b_conv3 = bias_variable([48])

h_conv3 = conv2d(h_conv2, W_conv3, 2, b_conv3, name = 'conv3')

#fourth convolutional layer
W_conv4 = weight_variable([3, 3, 48, 64])
b_conv4 = bias_variable([64])

h_conv4 = conv2d(h_conv3, W_conv4, 1, b_conv4, name = 'conv4')

#fifth convolutional layer
W_conv5 = weight_variable([3, 3, 64, 64])
b_conv5 = bias_variable([64])

h_conv5 = conv2d(h_conv4, W_conv5, 1, b_conv5, name = 'conv5')

#FCL 1
W_fc1 = weight_variable([1152, 1164])
b_fc1 = bias_variable([1164])

h_conv5_flat = tf.reshape(h_conv5, [-1, 1152])
h_fc1_drop = fc_layer(h_conv5_flat, W_fc1, b_fc1, keep_prob, name = 'FCL1')

#FCL 2
W_fc2 = weight_variable([1164, 100])
b_fc2 = bias_variable([100])

h_fc2_drop = fc_layer(h_fc1_drop, W_fc2, b_fc2, keep_prob, name = 'FCL2')

#FCL 3
W_fc3 = weight_variable([100, 50])
b_fc3 = bias_variable([50])

h_fc3_drop = fc_layer(h_fc2_drop, W_fc3, b_fc3, keep_prob, name = 'FCL3')

#FCL 4
W_fc4 = weight_variable([50, 10])
b_fc4 = bias_variable([10])

h_fc4_drop = fc_layer(h_fc3_drop, W_fc4, b_fc4, keep_prob, name = 'FCL4')

#Output
W_fc5 = weight_variable([10, 1])
b_fc5 = bias_variable([1])

y = output(h_fc4_drop, W_fc5, b_fc5)