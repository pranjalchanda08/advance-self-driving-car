'''********************************************************************************************************
'' Imports
''******************************************************************************************************'''
import numpy as np
import argparse
import imutils
import time
import cv2
import os
import sys
from   threading import Timer

'''********************************************************************************************************
'' @breief   		This function is used to initialise YOLO instance
'' 
'' @param[in]		yolo_attr		A dictionary containing labelsPath, configPath, weightsPath
'' @return			yolo_attr       Same dictionary with addition of LABELS, COLORS, net and layerNames
''******************************************************************************************************'''
def yoloInit(yolo_attr):
	'''Open label path file and read all the lables present in the file '''
	LABELS = open(yolo_attr['labelsPath']).read().strip().split("\n")
	np.random.seed(42)
	'''Associate colour for bounding boxes to each class labels '''
	COLORS = np.random.randint(0, 255, size=(len(LABELS), 3),
		dtype="uint8")
	print("[INFO] loading YOLO from disk...")
	'''Read the YOLO weight file and config file and get the Neural network '''
	net = cv2.dnn.readNetFromDarknet(yolo_attr['configPath'], yolo_attr['weightsPath'])
	'''Set the OpenCV environment as GPU based environment for better performance. '''
	net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
	net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
	'''Read all the layers present in the Neural Network '''
	layerNames = net.getLayerNames()
	layerNames = [layerNames[i[0] - 1] for i in net.getUnconnectedOutLayers()]
	'''Set the dictionary for returning it to the main program. ''' 
	yolo_attr['LABELS'], yolo_attr['COLORS'], yolo_attr['net'], yolo_attr['layerNames'] = LABELS, COLORS, net, layerNames
	return yolo_attr

'''********************************************************************************************************
'' @breief  		This function is used to detect objects in any image/frame 
'' 
'' @param[in]		image    		A numpy based input representing the frame/image
'' @return			yolo_attr       A dictionary containing all informations returned in initialisation
''******************************************************************************************************'''
def DetectObj(image, yolo_attr):
	'''blob is a preprocessed image of specific size and helps 
	OpenCV read BGR based frames and returns RGB formatted scalled image for the input to the Neural network.'''
	blob = cv2.dnn.blobFromImage(image, 1 / 255.0, (416, 416), swapRB=True, crop=False)
	'''Set the Neural network input as blob'''
	yolo_attr['net'].setInput(blob)
	'''Get predicted outputs using forward progression of the Neural network. Set it in the dictionary and return to main program'''
	yolo_attr['layerOutputs'] = yolo_attr['net'].forward(yolo_attr['layerNames'])	
	return yolo_attr

'''********************************************************************************************************
'' @breief  		This function is used to create bounding boxes on the frame according to prediction
'' 
'' @param[in]		image    		Frame/image which needs to be visualised
'' @param[in/out]	boxes			A list containing boxes or default is emplty list
'' @param[in/out]	confidences		A list containing all the confidence levels of respective box predictions
'' @param[in/out]	classIDs		A list of predicted classIDs or default is emplty list.
'' @param[in]		yolo_attr		A dictionary containing all informations returned in initialisation and detection
'' @return			image 	        New image with drawn box
'' @return 			cordinates		A tuple of bounding box cordinates in the format (x,y,w,h) 
''******************************************************************************************************'''
def visualise(image, boxes, confidences, classIDs, yolo_attr):
	'''Get the width and height of input image'''
	(H, W) = image.shape[:2]
	'''Set null list for cordinates'''
	cordinates = []
	'''Iterate through outputs provided by the neural network'''
	for output in yolo_attr['layerOutputs']:
		'''Iterate through each output'''
		for detection in output:
			'''Get the actual prediction score/percentage from a scale of 0->1'''
			scores = detection[5:]
			'''Get the classID with highest score'''
			classID = np.argmax(scores)
			'''Get the max score value as confidence'''
			confidence = scores[classID]
			'''Keeping 0.5 as confidence threshold get the bounding boxes'''
			if confidence > 0.5:
				'''Get the box cordinates with respect to image height and width'''
				box = detection[0:4] * np.array([W, H, W, H])
				'''Get all the box cordinates as intiigers'''
				(centerX, centerY, width, height) = box.astype("int")
				'''Some Mathematics :P'''
				x = int(centerX - (width / 2))
				y = int(centerY - (height / 2))
				'''Append all the predicted boxes in a list'''
				boxes.append([x, y, int(width), int(height)])
				'''Append all the predicted confidences in a list'''
				confidences.append(float(confidence))
				'''Append all the predicted classID in a list'''
				classIDs.append(classID)
	'''
	''# Performs non maximum suppression given boxes and corresponding scores 
	''	  Ref: https://kite.com/python/docs/cv2.dnn.NMSBoxes
	''# What is Non-Maximal Suppression (NMS)?
	''	  YOLO uses Non-Maximal Suppression (NMS) to only keep the best bounding box. 
	''	  The first step in NMS is to remove all the predicted bounding boxes that have a detection probability that is less than a given NMS threshold. 
	''	  In the code below, we set this NMS threshold to 0.5. 
	''	  This means that all predicted bounding boxes that have a detection probability less than 0.5 will be removed.'''
	idxs = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.3)
	if len(idxs) > 0:
		'''Iterate over all boxes'''
		for i in idxs.flatten():
			'''Get the cordinates of the selected index out of boxes list'''
			(x, y) = (boxes[i][0], boxes[i][1])
			(w, h) = (boxes[i][2], boxes[i][3])
			'''Lets draw the bounding boxes'''
			color = [int(c) for c in yolo_attr['COLORS'][classIDs[i]]]
			cv2.rectangle(image, (x, y), (x + w, y + h), color, 1)
			text = "{}: {:.0f}%".format(yolo_attr['LABELS'][classIDs[i]], (confidences[i]*100))
			cv2.putText(image, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX,
				0.3, color, 1)
			'''Append cordinates as a tuple for distance estimation'''
			cordinates.append(tuple([x,y,w,h]))
	return image, cordinates