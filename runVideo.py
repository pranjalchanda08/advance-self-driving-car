import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import scipy.misc
import cv2
import argparse
import numpy as np
import math
import os
import yolov3 as od
import sys
import json 
from   time import sleep
from   threading import Timer
from   Steering import model
from   subprocess import call

Resources = "./Resources"

sess = tf.InteractiveSession()
saver = tf.train.Saver()
saver.restore(sess, "Steering/save/model.ckpt")

smoothed_angle = 0
pos_global_slopes = pos_global_intercepts = neg_global_slopes = neg_global_intercepts = np.array([])
top_y = np.array([])

parser = argparse.ArgumentParser()
parser.add_argument('--vSrc', metavar = 'S', type=str, help= 'Path of video. Default: webcam', default = 0)
parser.add_argument('--model', metavar = 'M', type=str, help= 'Model name. Default: yolov3', default = 'yolov3')

fps = 0
capturefps = 0
interrupt = False

def PrintSameLine(str):
    sys.stdout.write('\r')
    sys.stdout.write(str)
    sys.stdout.flush()

def captureFpS():
    global interrupt
    interrupt = True

def predictSteer(img):
    steering_wheel_path = os.path.join(Resources, 'steering_wheel_image.jpg')
    img_stear = cv2.imread(steering_wheel_path)
    rows,cols = img_stear.shape[:2]
    global smoothed_angle
    img = cv2.resize(img, (640,360))
    image = cv2.resize(img[-150:], (200,66)) / 255.0
    degrees = model.y.eval(feed_dict={model.x: [image], model.keep_prob: 1.0})[0][0] * 180.0 / scipy.pi
    smoothed_angle += 0.2 * pow(abs((degrees - smoothed_angle)), 2.0 / 3.0) * (degrees - smoothed_angle) / abs(degrees - smoothed_angle)
    M = cv2.getRotationMatrix2D((cols/2,rows/2),-smoothed_angle,1)
    dst = cv2.warpAffine(img_stear,M,(cols,rows))
    dst = cv2.resize(dst, (int(rows/2), int(cols/2)))
    return smoothed_angle, dst

def CombineInfo(src, steerAngle, wheel_img, fps):
    (h,w) = src.shape[:2]
    dst = np.copy(src)
    font = cv2.FONT_HERSHEY_DUPLEX
    org = (50, 50) 
    thickness = 1
    color = (255, 0, 0) 
    fontScale = 0.5
    direction = 'Straight'
    if (steerAngle > -10) and (steerAngle < 10):
        direction = 'Straight'
    elif(steerAngle < -10):
        direction = 'Left'
    else:
        direction = 'Right'
    if steerAngle < 0:
        steerAngle *= (-1)
    text = 'Steering Angle: ' + '{:04.2f}'.format(steerAngle) + 'degree'
    dst = cv2.putText(dst, text, org, font, fontScale, color, thickness, cv2.LINE_AA)
    text = 'FPS: ' + '{}'.format(fps)
    org = (w-150, 50)
    color = (255, 100, 0)
    dst = cv2.putText(dst, text, org, font, fontScale, color, thickness, cv2.LINE_AA)
    text = 'Direction: ' + direction
    org = (50, 70)
    color = (0, 100, 0)
    dst = cv2.putText(dst, text, org, font, fontScale, color, thickness, cv2.LINE_AA)
    overlay = np.zeros((src.shape[0],src.shape[1],src.shape[2]), dtype='uint8')
    for i in range(0,wheel_img.shape[0]):
        for j in range(0,wheel_img.shape[1]):
            overlay[i + (src.shape[0] - 20) - wheel_img.shape[0] ,j + (src.shape[1] - 50) - wheel_img.shape[1]] = wheel_img[i,j]    
    dst = cv2.addWeighted(overlay, 1, dst, 1, 0, 0)
    return dst

def LaneLine(image, points):
    if type(points) != list:
        print("Points are supposed to be list")
    converted = yellow_to_white(image)
    max_width = image.shape[1]
    max_height = image.shape[0]
    sample_image = np.copy(converted)
    width_delta = int(max_width/50)
    height_delta = 30
    vertex=[(points[0],points[1]),(points[2],points[3]),(points[4],points[5]),(points[6],points[7])]
    vertices = np.array([vertex], np.int32)
    gray_scaled = grayscale(sample_image)
    blurred_img = gaussian_blur(gray_scaled, 5)
    cimage = canny(blurred_img, 10, 20)
    selected = region_of_interest(cimage, vertices)
    fin = hough_lines(selected, 1, 1 * np.pi/180, 20, 10, 10)
    result = weighted_img(fin, image, α=0.8, β=1., λ=0.)
    return result, fin, selected, cimage, blurred_img


def grayscale(img):
    return cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

def auto_canny(image, sigma=0.8):
    v = np.median(image)
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    auto = canny(image, lower, upper)
    return auto

def canny(img, low_threshold, high_threshold):
    return cv2.Canny(img, low_threshold, high_threshold)

def gaussian_blur(img, kernel_size):
    return cv2.GaussianBlur(img, (kernel_size, kernel_size), 0)

def region_of_interest(img, vertices):
    mask = np.zeros_like(img)   
    if len(img.shape) > 2:
        channel_count = img.shape[2]  # i.e. 3 or 4 depending on your image
        ignore_mask_color = (255,) * channel_count
    else:
        ignore_mask_color = 255
    cv2.fillPoly(mask, vertices, ignore_mask_color)
    masked_image = cv2.bitwise_and(img, mask)
    return masked_image

def yellow_to_white(img):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    upper_yellow = np.array([100, 255, 255]) 
    lower_yellow = np.array([80, 100, 100]) 
    mask_inverse = cv2.inRange(hsv, lower_yellow, upper_yellow)    
    masked_replace_white = cv2.addWeighted(img, 1, \
                                       cv2.cvtColor(mask_inverse, cv2.COLOR_GRAY2RGB), 1, 0)
    return masked_replace_white

def draw_lines(img, lines, color=[0, 255, 0], thickness=5):
    for line in lines:        
        for x1,y1,x2,y2 in line:
            cv2.line(img, (x1, y1), (x2, y2), color, thickness)

def hough_lines(img, rho, theta, threshold, min_line_len, max_line_gap):
    lines = cv2.HoughLinesP(img, rho, theta, threshold, np.array([]), minLineLength=min_line_len, maxLineGap=max_line_gap)  
    line_img = np.zeros((*img.shape, 3), dtype=np.uint8)
    extrapolate(line_img, lines)
    return line_img

def lines_linreg(lines_array):  
    try:
        x = np.reshape(lines_array[:, [0, 2]], (1, len(lines_array) * 2))[0]
        y = np.reshape(lines_array[:, [1, 3]], (1, len(lines_array) * 2))[0]
        A = np.vstack([x, np.ones(len(x))]).T
        m, c = np.linalg.lstsq(A, y)[0]
        x = np.array(x)
        y = np.array(x * m + c).astype('int')
    except:
        print(x,y)
    return x, y, m, c

def mad(data, axis=None):
    return np.mean(np.abs(data - np.mean(data, axis)), axis)

def extrapolate(img, lines): 
    global pos_global_slopes
    global pos_global_intercepts
    global neg_global_slopes
    global neg_global_intercepts
    global top_y
    max_height = img.shape[0]
    max_width = img.shape[1]
    if lines is None :
        print("******************* No lines detected")
        return
    slopes = np.apply_along_axis(lambda row: (row[3] - row[1])/(row[2] - row[0]), 2, lines)
    pos_slopes = slopes > 0.50
    pos_lines = lines[pos_slopes]
    neg_slopes = slopes < -0.50
    neg_lines = lines[neg_slopes]
    if len(pos_lines) == 0 or len(neg_lines) == 0:
        return;
    pos_x, pos_y, pos_m, pos_c = lines_linreg(pos_lines)
    pos_global_slopes = np.append(pos_global_slopes, [pos_m])
    pos_global_intercepts = np.append(pos_global_intercepts, [pos_c])
    pos_m = pos_global_slopes[-20:].mean()
    pos_c = pos_global_intercepts[-20:].mean()
    neg_x, neg_y, neg_m, neg_c = lines_linreg(neg_lines)
    neg_global_slopes = np.append(neg_global_slopes, [neg_m])
    neg_global_intercepts = np.append(neg_global_intercepts, [neg_c])
    neg_m = neg_global_slopes[-20:].mean()
    neg_c = neg_global_intercepts[-20:].mean()
    bottom_left_y = img.shape[0]
    bottom_left_x = int((bottom_left_y - neg_c)/neg_m)
    min_top_y = np.min([neg_y.min(), pos_y.min()])
    top_y = np.append(top_y, [min_top_y])
    min_top_y = int(top_y[-20:].mean())
    top_left_y = min_top_y
    top_left_x = int((top_left_y - neg_c)/neg_m)
    top_right_y = min_top_y
    top_right_x = int((top_right_y - pos_c)/pos_m)
    bottom_right_y = img.shape[0]
    bottom_right_x = int((bottom_right_y - pos_c)/pos_m)
    #Average    
    cv2.line(img, (bottom_left_x, bottom_left_y), (top_left_x, top_left_y), [255, 125, 0], 3)
    cv2.line(img, (top_right_x, top_right_y), (bottom_right_x, bottom_right_y), [255, 125, 0], 3)

def weighted_img(img, initial_img, α=0.8, β=1., λ=0.):
    return cv2.addWeighted(initial_img, α, img, β, λ)

def distanceVisualise(img, cordinates):
    img_dist = np.copy(img)
    img_cpy = np.copy(img)
    h,w = img_dist.shape[:2]
    font = cv2.FONT_HERSHEY_DUPLEX                    
    org = (50, 50) 
    thickness = 1
    fontScale = 0.5
    mw = w // 2
    mh = h // 2
    wrapoffset = 200  
    off = 30  
    src = np.array([((mw - off), mh + 50),((mw + off), mh + 50), (w,h) ,(0,h)], dtype = "float32")
    dst = np.array([(wrapoffset, 0),(w - wrapoffset, 0),(w - wrapoffset, h),(wrapoffset, h)], dtype = "float32")
    matrix = cv2.getPerspectiveTransform(src, dst)
    bev = cv2.warpPerspective(img_cpy, matrix, (w,h), flags=cv2.INTER_LINEAR) 
    for cordinate in cordinates:
        mid_x = (cordinate[0] + (cordinate[2] // 2))
        mid_y = (cordinate[1] + (cordinate[3] // 2))
        mid_w = mid_x 
        mid_h = cordinate[1] + cordinate[3]
        ego_bev_xy = w // 2, h
        p = (int(mid_w), int(mid_h))    
        cv2.line(img_dist, p, ego_bev_xy, color=(255, 255, 255), thickness=2)
        px = int((matrix[0][0]*p[0] + matrix[0][1]*p[1] + matrix[0][2]) / ((matrix[2][0]*p[0] + matrix[2][1]*p[1] + matrix[2][2])))
        py = int((matrix[1][0]*p[0] + matrix[1][1]*p[1] + matrix[1][2]) / ((matrix[2][0]*p[0] + matrix[2][1]*p[1] + matrix[2][2])))
        delta = [px, py] - np.asarray(ego_bev_xy)
        dist_pix = np.sqrt(np.sum(delta ** 2))
        distance = dist_pix/20 
        cv2.putText(img_dist, '{:.1f}m'.format(distance), (mid_x , mid_y), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (255,255,255), 1)            
    cv2.putText(bev, "Bird's Eye View", org, font, fontScale, (0, 255, 0) , thickness, cv2.LINE_AA)
    cv2.putText(img_dist, "Relative Distance", org, font, fontScale, (0, 0, 255) , thickness, cv2.LINE_AA)
    return img_dist, bev

def main():
    args = parser.parse_args()
    t = Timer(1, captureFpS)
    global interrupt
    points = None
    if args.vSrc == 0:
        src = 0
    else:
        json_path = os.path.join(Resources, 'register.json')
        print("searching for: " + args.vSrc)
        with open(json_path,'r') as json_file: 
            videos = json.load(json_file)
            for video in videos:
                print(video['name'])
                if video['name'] == args.vSrc:
                    src = video[video['name']]['path']
                    points = video[video['name']]['ROI']
                    break
            if points == None:
                print("Video not registered in json file.")
                sys.exit(0)                
    print("src = {}".format(src))
    cap = cv2.VideoCapture(src)
    ret, img_org = cap.read()
    i = 0
    fps = 0
    captureFPS = 0
    t.start()
    yolo_PATH_BASE = 'yolo-coco'
    yolo_attr = {}
    yolo_attr['labelsPath']  = os.path.sep.join([yolo_PATH_BASE, "coco.names"])
    yolo_attr['weightsPath'] = os.path.sep.join([yolo_PATH_BASE, args.model+".weights"])
    yolo_attr['configPath']  = os.path.sep.join([yolo_PATH_BASE,  args.model+".cfg"])
    yolo_attr = od.yoloInit(yolo_attr)
    while(cv2.waitKey(1) != ord('q')):
        if not ret:
            break;
        img = cv2.resize(img_org, (640,480))
        if interrupt == True:
            interrupt = False
            captureFPS = fps
            fps = 0
            t.cancel()
            t = Timer(1, captureFpS)
            t.start()
        original_image = np.copy(img)
        if (i % 5 == 0):
            yolo_attr = od.DetectObj(img, yolo_attr)
            boxes = confidences = []
            confidences = []
            classIDs = []
            smoothed_angle, wheel_img = predictSteer(img)
            PrintSameLine("smoothed_angle: {}".format(smoothed_angle))
        result, fin, selected, cimage, blurred_img = LaneLine(img, points = points)
        result = CombineInfo(result, smoothed_angle, wheel_img, captureFPS)                      
        img_show = np.copy(img) 
        img_show, cordinates = od.visualise(img_show, boxes, confidences, classIDs, yolo_attr)
        img_dist, bev = distanceVisualise(img, cordinates)
        cv2.putText(img_show, "Object Detection", (50, 50), cv2.FONT_HERSHEY_DUPLEX, 0.5, (125, 0, 120) , 1, cv2.LINE_AA)
        numpy_horizontal1 = np.hstack((result, img_show))
        numpy_horizontal2 = np.hstack((bev, img_dist))
        numpy_vertical    = np.vstack((numpy_horizontal1, numpy_horizontal2))
        cv2.imshow("Out", cv2.resize(numpy_vertical, (1850,1000)))
        ret, img_org = cap.read()
        i = i + 1
        fps =  fps + 1
    cv2.destroyAllWindows()

if __name__ == '__main__':
	main()