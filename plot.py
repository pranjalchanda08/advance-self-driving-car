import matplotlib.pyplot as plt 

x = []
y = []
with open('log.txt', 'r') as f:
	while True:
		try:
			x.append(float(f.readline()))
		except Exception as e:
			break
		
for i,j in enumerate(x):
	y.append(i)
plt.plot(y, x, label= "loss", color= "blue") 
plt.xlabel('Itterations') 
plt.ylabel('Loss') 
plt.title('Loss Graph') 
plt.legend() 
plt.show() 